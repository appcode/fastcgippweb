#!/bin/bash

ulimit -c unlimited
ulimit -n 1048576 
#################################################################
# only modify this items below usually
#################################################################

cgi_name="test"
cgi_suffix=".fcgi"
proc_num=1

#################################################################
# do not modify this below usully
#################################################################

killall -9 ${cgi_name}${cgi_suffix}
