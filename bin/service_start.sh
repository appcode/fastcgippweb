#!/bin/bash

ulimit -c unlimited
ulimit -n 1048576 

#################################################################
# only modify this items below usually
#################################################################

cgi_name="test"
cgi_suffix=".fcgi"
proc_num=1

#################################################################
# do not modify this below usully
#################################################################

dir=`pwd`

spawn_name="spawn-fcgi"
spawn=${dir}/${spawn_name}
cgi=${dir}/${cgi_name}${cgi_suffix}
sock="/dev/shm/"${cgi_name}${cgi_suffix}".sock"
pid=${dir}/${cgi_name}".pid"

# 修改启动时验证服务是否已经正在运行
#count=`ps aux | grep ${cgi_name}${cgi_suffix} | grep -v "grep" | wc -l`
count=`ps aux | grep ${cgi} | grep -v "grep" | wc -l`

if [ ${count} -gt 0 ] ; then
	echo "${cgi_name}${cgi_suffix} is already running!"
	exit
fi

${spawn} -F ${proc_num} -f ${cgi} -s ${sock} -P ${pid}
chmod 777 ${sock}
