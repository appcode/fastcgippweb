######################################################################
# Automatically generated by qmake (3.0) ?? 5? 20 21:32:39 2016
######################################################################

TEMPLATE = app
TARGET = fastcgippweb
INCLUDEPATH += . \
                include \

# Input
HEADERS += include/AdUrlHandler.h \
           include/CacheConnectionPool.h \
           include/CConnectionPool.h \
           include/ConfigFileReader.h \
           include/Context.h \
           include/DBPool.h \
           include/DbUtil.h \
           include/FastCgiHandler.h \
           include/HandlerMapping.h \
           include/HttpHandler.h \
           include/HttpHandlerManage.h \
           include/IniFile.h \
           include/RedisConnetionPool.h \
           include/ServerContext.h \
           include/TestHandler.h \
           include/Thread.h \
           include/util.h \
           include/hiredis/async.h \
           include/hiredis/dict.h \
           include/hiredis/fmacros.h \
           include/hiredis/hiredis.h \
           include/hiredis/net.h \
           include/hiredis/sds.h \
           include/hiredis/sdsalloc.h \
           include/hiredis/zmalloc.h \
           include/rapidjson/allocators.h \
           include/rapidjson/document.h \
           include/rapidjson/encodedstream.h \
           include/rapidjson/encodings.h \
           include/rapidjson/filereadstream.h \
           include/rapidjson/filewritestream.h \
           include/rapidjson/memorybuffer.h \
           include/rapidjson/memorystream.h \
           include/rapidjson/prettywriter.h \
           include/rapidjson/rapidjson.h \
           include/rapidjson/reader.h \
           include/rapidjson/stringbuffer.h \
           include/rapidjson/writer.h \
           include/hiredis/adapters/ae.h \
           include/hiredis/adapters/libev.h \
           include/hiredis/adapters/libevent.h \
           include/hiredis/adapters/libuv.h \
           include/rapidjson/error/en.h \
           include/rapidjson/error/error.h \
           include/rapidjson/internal/biginteger.h \
           include/rapidjson/internal/diyfp.h \
           include/rapidjson/internal/dtoa.h \
           include/rapidjson/internal/ieee754.h \
           include/rapidjson/internal/itoa.h \
           include/rapidjson/internal/meta.h \
           include/rapidjson/internal/pow10.h \
           include/rapidjson/internal/stack.h \
           include/rapidjson/internal/strfunc.h \
           include/rapidjson/internal/strtod.h \
           include/rapidjson/msinttypes/inttypes.h \
           include/rapidjson/msinttypes/stdint.h \
           /include/CConnectionPool.h \
           /include/ServerContext.h \
           /include/TestHandler.h \
           /include/AdUrlHandler.h \
           /include/DBPool.h \
           /include/DbUtil.h \
           /include/IniFile.h \
           /include/rapidjson/document.h \
           /include/rapidjson/msinttypes/stdint.h \
           /include/rapidjson/msinttypes/inttypes.h \
           /include/rapidjson/prettywriter.h \
           /include/rapidjson/filereadstream.h \
           include/hiredis/dict.c \
           /include/ConfigFileReader.h \
           /include/RedisConnetionPool.h \
           /include/hiredis/adapters/ae.h \
           /include/hiredis/adapters/libev.h \
           /include/hiredis/adapters/libevent.h \
           /include/hiredis/adapters/libuv.h \
    include/spnetkit/spnkhttpcli.hpp \
    include/spnetkit/spnkhttpmsg.hpp \
    include/spnetkit/spnkporting.hpp \
    include/spnetkit/spnksslsocket.hpp \
    include/spnetkit/spnksocket.hpp \
    include/MD5.h \
    include/fastcgi++/exceptions.hpp \
    include/fastcgi++/fcgistream.hpp \
    include/fastcgi++/http.hpp \
    include/fastcgi++/manager.hpp \
    include/fastcgi++/message.hpp \
    include/fastcgi++/protocol.hpp \
    include/fastcgi++/request.hpp \
    include/fastcgi++/transceiver.hpp \
    include/spnetkit/spnkbase64.hpp \
    include/spnetkit/spnkbuffer.hpp \
    include/spnetkit/spnkconfig.hpp \
    include/spnetkit/spnkendpoint.hpp \
    include/spnetkit/spnkfile.hpp \
    include/spnetkit/spnkhash.hpp \
    include/spnetkit/spnkhttpsvr.hpp \
    include/spnetkit/spnkhttputils.hpp \
    include/spnetkit/spnkicapcli.hpp \
    include/spnetkit/spnkini.hpp \
    include/spnetkit/spnklist.hpp \
    include/spnetkit/spnklock.hpp \
    include/spnetkit/spnklog.hpp \
    include/spnetkit/spnkmd5.hpp \
    include/spnetkit/spnkmemcli.hpp \
    include/spnetkit/spnkmemobj.hpp \
    include/spnetkit/spnkmiltercli.hpp \
    include/spnetkit/spnkpop3cli.hpp \
    include/spnetkit/spnkprefork.hpp \
    include/spnetkit/spnkreader.hpp \
    include/spnetkit/spnkserver.hpp \
    include/spnetkit/spnksmtpaddr.hpp \
    include/spnetkit/spnksmtpcli.hpp \
    include/spnetkit/spnksocketpool.hpp \
    include/spnetkit/spnksslsmtpcli.hpp \
    include/spnetkit/spnkstr.hpp \
    include/spnetkit/spnkthread.hpp \
    include/spnetkit/spnktime.hpp \
    include/RapidJsonMacro.h \
    include/fastcgi++/config.h \
    include/fastcgi++/fcgistreambuf.hpp \
    include/fastcgi++/log.hpp \
    include/fastcgi++/sockets.hpp \
    include/fastcgi++/webstreambuf.hpp \
    include/Hashtable.h \
    include/MemPool.h \
    include/Singleton.h \
    include/GloabData.h \
    include/UseRegHandler.h \
    include/controller/AdUrlHandler.h \
    include/controller/Handler.h \
    include/controller/TestHandler.h \
    include/controller/UserRegHandler.h \
    include/Handler.h \
    include/Model.h \
    include/model/UserModel.h \
    include/ModelBase.h \
    include/bean/User.h \
    include/ClassMacro.h \
    include/controller/UserLoginHandler.h
SOURCES += src/main.cpp \
           test/test.cpp \
           src/comm/CConnectionPool.cpp \
           src/comm/ConfigFileReader.cpp \
           src/comm/DBPool.cpp \
           src/comm/DbUtil.cpp \
           src/comm/FastCgiHandler.cpp \
           src/comm/HandlerMapping.cpp \
           src/comm/HttpHandlerManage.cpp \
           src/comm/IniFile.cpp \
           src/comm/RedisConnectionPool.cpp \
           src/comm/ServerContext.cpp \
           src/comm/Thread.cpp \
           src/comm/util.cpp \
           src/controller/AdUrlHandler.cpp \
           src/controller/TestHandler.cpp \
    src/comm/MD5.cpp \
    include/hiredis/examples/example-ae.c \
    include/hiredis/examples/example-libev.c \
    include/hiredis/examples/example-libevent.c \
    include/hiredis/examples/example-libuv.c \
    include/hiredis/examples/example.c \
    include/hiredis/async.c \
    include/hiredis/hiredis.c \
    include/hiredis/net.c \
    include/hiredis/sds.c \
    include/hiredis/test.c \
    src/comm/MemPool.cpp \
    src/comm/GloabData.cpp \
    src/controller/UserRegHandler.cpp \
    src/model/UserModel.cpp \
    src/controller/UserLoginHandler.cpp
