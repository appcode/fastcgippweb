#ifndef __H_DBUTIL__
#define __H_DBUTIL__

#include <string>

#include <mysql++/mysql++.h>
#include <mysql++/cpool.h>

class DbUtil {

public:
	static mysqlpp::StoreQueryResult Select(mysqlpp::ConnectionPool* pool,const std::string& sql);
	static int Insert(mysqlpp::ConnectionPool* pool,const std::string& sql);
	static int Update(mysqlpp::ConnectionPool* pool,const std::string& sql);
	static int Delete(mysqlpp::ConnectionPool* pool,const std::string& sql);
};

#endif