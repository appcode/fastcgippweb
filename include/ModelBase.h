#ifndef __H_MODEL_BASE__
#define __H_MODEL_BASE__

#include "DBPool.h"
#include "CacheConnectionPool.h"
#include "Context.h"
#include <memory>

class ModelBase {

public:
    ModelBase(){};
    virtual ~ModelBase(){};

protected:
//    const char* db_pool_name;
//    const char* cache_pool_name;
    CDBPool* dbpool;
    CacheConnectionPool* cpool;
    Context* ctx;
};

#endif
