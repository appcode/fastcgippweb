// MD5.h: interface for the MD5 class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MD5_H__6B2CC99A_2AF9_410B_BB88_16BA2510349B__INCLUDED_)
#define AFX_MD5_H__6B2CC99A_2AF9_410B_BB88_16BA2510349B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <string.h>
#include <iostream>
#include <stdio.h>
class MD5
{
public:
	MD5();
	virtual ~MD5();
	void digest_md5(unsigned char const *buf, unsigned int len, unsigned char digest[16]);
	static std::string getMd5( std::string strBuf );
protected:
	struct MD5Context
	{
		unsigned int buf[4];
		unsigned int bits[2];
		unsigned char in[64];
	};
	void MD5Init (MD5Context *ctx);
	void MD5Update (MD5Context *ctx,unsigned char const *buf, unsigned int len);
	void MD5Final (unsigned char digest[16], MD5Context *context);
	void MD5Transform (unsigned int buf[4], unsigned int const in[16]);
	MD5Context m_ctx;
};

#endif // !defined(AFX_MD5_H__6B2CC99A_2AF9_410B_BB88_16BA2510349B__INCLUDED_)
