#ifndef __H_SERVER_CONTEXT__
#define __H_SERVER_CONTEXT__

#include <string>
#include <string.h>
#include <map>

#include "Context.h"

class ServerContext : public Context {

public:
	ServerContext();
	virtual ~ServerContext();

	static Context* getInstance();

    virtual int initContext();

	virtual void setContext(const char* key,void* value);
	virtual void setContext(const std::string& key,void* value);

	virtual void* getContext(const char* key);
	virtual void* getContext(const std::string& key);

private:
	std::map<std::string,void*> contextMap;

};

#endif
