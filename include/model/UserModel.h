#ifndef __H_USER_MODEL__
#define __H_USER_MODEL__

#include <string>
#include "ModelBase.h"
#include "bean/User.h"

class UserModel : public ModelBase {
public:
    UserModel(const char* db_pool_name,const char* cache_pool_name);
    ~UserModel();

    int userIsExist(const char* user_name);
    int addUser(User* user);
    bool getUser(const char* username,User& user);
    bool setTokenCache(const char* username,const char* token);
};

#endif
