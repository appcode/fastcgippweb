#ifndef __H_HANDLER_MAPPING__
#define __H_HANDLER_MAPPING__

#include <map>

class FastCgiHandler;


class HandlerMapping {

public:
	static HandlerMapping* getInstance();
	
	void addUrlMappingHandler(const std::string& url,FastCgiHandler* fastCgiHandler);
	FastCgiHandler* getHandler(const std::string& url);

private:
	std::map<std::string,FastCgiHandler*> handlerMapping;

	HandlerMapping();
};

#endif