#ifndef __H_HTTP_HANDLER_MANAGE__
#define __H_HTTP_HANDLER_MANAGE__

#include "HandlerMapping.h"

class HttpHandlerManage {
public:
	HttpHandlerManage();
	~HttpHandlerManage();

	void addHandlerMapping(const std::string& url,FastCgiHandler* fastCgiHandler);
	FastCgiHandler* getHandler(const std::string& url);

private:
	HandlerMapping* handlerMapping;
};

#endif
