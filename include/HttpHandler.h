#ifndef __H_HTTP_HANDLER__
#define __H_HTTP_HANDLER__

#include <fastcgi++/request.hpp>

class HttpHandler {

public:
	HttpHandler();
	virtual ~HttpHandler();

	virtual void init();

	virtual void handler(Fastcgipp::Request<char>& req);
	virtual void doGet(Fastcgipp::Request<char>& req);
	virtual void doPost(Fastcgipp::Request<char>& req);
};

#endif