#ifndef __H_FASTCGI_HANDLER__
#define __H_FASTCGI_HANDLER__

#include <fastcgi++/request.hpp>

#include "HttpHandlerManage.h"

class FastCgiHandler {
	
public:
    FastCgiHandler(const std::string& url);
	virtual ~FastCgiHandler();

    virtual void init(const std::string& url);
//    virtual void handle(Fastcgipp::Request<char>& req,Fastcgipp::Http::Environment<char>& env);
//    virtual void doGet(Fastcgipp::Request<char>& req);
//    virtual void doPost(Fastcgipp::Request<char>& req);

    virtual void doGet(const Fastcgipp::Http::Environment<char>& env,std::basic_ostream<char>& resp);
    virtual void doPost(const Fastcgipp::Http::Environment<char>& env,std::basic_ostream<char>& resp);

private:
	HttpHandlerManage* httpHandlerManage;
};

#endif
