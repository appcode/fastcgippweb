#ifndef CACHECONNECTIONPOOL_H
#define CACHECONNECTIONPOOL_H

class CacheConnection {
public:
    CacheConnection(){};
    virtual ~CacheConnection(){};

    virtual int init() = 0;

    virtual bool get(const char* key,std::string& value) = 0;
    virtual bool set(const char* key,const char* value) = 0;
    virtual bool set(const char* key,const char* value,int ex) = 0;
    virtual bool isEffective() = 0;
};

class CacheConnectionPool
{
public:
    CacheConnectionPool(){};
    virtual ~CacheConnectionPool(){};
    virtual int init() = 0;

    virtual CacheConnection* getCacheConnection() = 0;
    virtual void putCacheConnection(CacheConnection* conn) = 0;
};

#endif // CACHECONNECTIONPOOL_H
