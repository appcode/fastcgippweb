#ifndef __H_USER_LOGIN_HANDLER__
#define __H_USER_LOGIN_HANDLER__

#include "FastCgiHandler.h"

class UserLoginHandler : public FastCgiHandler {
public:
    UserLoginHandler(const std::string& url);
    ~UserLoginHandler();

    void doGet(const Fastcgipp::Http::Environment<char>& env,std::basic_ostream<char>& resp);
    void doPost(const Fastcgipp::Http::Environment<char>& env,std::basic_ostream<char>& resp);
};

#endif
