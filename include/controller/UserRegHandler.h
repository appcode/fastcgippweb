#ifndef USERREGHANDLER_H
#define USERREGHANDLER_H

#include "FastCgiHandler.h"

class UserRegHandler : public FastCgiHandler{

public:
    UserRegHandler(const std::string& url);
    virtual ~UserRegHandler();

    virtual void doGet(const Fastcgipp::Http::Environment<char>& env,std::basic_ostream<char>& resp);
    virtual void doPost(const Fastcgipp::Http::Environment<char>& env,std::basic_ostream<char>& resp);
};

#endif // USERREGHANDLER_H
