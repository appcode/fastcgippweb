#ifndef __H_TEST_HANDLE__
#define __H_TEST_HANDLE__

#include "FastCgiHandler.h"

class TestHandler : public FastCgiHandler{

public:
    TestHandler(const std::string& url);
	virtual ~TestHandler();

    virtual void doGet(const Fastcgipp::Http::Environment<char>& env,std::basic_ostream<char>& resp);
    virtual void doPost(const Fastcgipp::Http::Environment<char>& env,std::basic_ostream<char>& resp);
	
};

#endif
