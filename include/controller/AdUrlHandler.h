#ifndef __H_ADURL_HANDLER__
#define __H_ADURL_HANDLER__

#include "FastCgiHandler.h"

class AdUrlHandler : public FastCgiHandler{

public:
    AdUrlHandler(const std::string& url);
	virtual ~AdUrlHandler();

//	virtual void doGet(Fastcgipp::Request<char>& req);
//	virtual void doPost(Fastcgipp::Request<char>& req);

    virtual void doGet(const Fastcgipp::Http::Environment<char>& env,std::basic_ostream<char>& resp);
    virtual void doPost(const Fastcgipp::Http::Environment<char>& env,std::basic_ostream<char>& resp);
	
}; 

#endif
