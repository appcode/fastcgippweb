#ifndef __H_CLASS_MACRO__
#define __H_CLASS_MACRO__

#define ClassPropertyBuildByName(type,name,permission) \
	permission:\
		type m_##name; \
    public: \
		inline void set##name(type t) {	\
            m_##name = t; \
        } \
		inline type get##name() {	\
            return m_##name; \
        } \

#endif
