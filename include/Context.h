#ifndef __H_CONTEXT__
#define __H_CONTEXT__

class Context {

public:
	virtual ~Context(){};

    virtual int initContext() = 0;
	virtual void setContext(const char* key,void* value) = 0;
	virtual void setContext(const std::string& key,void* value) = 0;

	virtual void* getContext(const char* key) = 0;
	virtual void* getContext(const std::string& key) = 0; 
};

#endif
