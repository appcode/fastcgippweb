#ifndef __H_RAPIDJSON_MACRO__
#define __H_RAPIDJSON_MACRO__

#include <rapidjson/document.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/filereadstream.h>

// 基础变量的校验
#define json_check_is_bool(value, strKey) (value.HasMember(strKey) && value[strKey].IsBool())
#define json_check_is_string(value, strKey) (value.HasMember(strKey) && value[strKey].IsString())
#define json_check_is_int32(value, strKey) (value.HasMember(strKey) && value[strKey].IsInt())
#define json_check_is_uint32(value, strKey) (value.HasMember(strKey) && value[strKey].IsUint())
#define json_check_is_int64(value, strKey) (value.HasMember(strKey) && value[strKey].IsInt64())
#define json_check_is_uint64(value, strKey) (value.HasMember(strKey) && value[strKey].IsUint64())
#define json_check_is_float(value, strKey) (value.HasMember(strKey) && value[strKey].IsFloat())
#define json_check_is_double(value, strKey) (value.HasMember(strKey) && value[strKey].IsDouble())

#define json_check_is_number(value, strKey) (value.HasMember(strKey) && value[strKey].IsNumber())
#define json_check_is_array(value, strKey) (value.HasMember(strKey) && value[strKey].IsArray())

// 得到对应类型的数据，如果数据不存在则得到一个默认值
#define json_check_bool(value, strKey) (json_check_is_bool(value, strKey) && value[strKey].GetBool())
#define json_check_string(value, strKey) (json_check_is_string(value, strKey) ? value[strKey].GetString() : "")
#define json_check_int32(value, strKey) (json_check_is_int32(value, strKey) ? value[strKey].GetInt() : 0)
#define json_check_uint32(value, strKey) (json_check_is_uint32(value, strKey) ? value[strKey].GetUint() : 0)
#define json_check_int64(value, strKey) (json_check_is_int64(value, strKey) ? ((value)[strKey]).GetInt64() : 0)
#define json_check_uint64(value, strKey) (json_check_is_uint64(value, strKey) ? ((value)[strKey]).GetUint64() : 0)
#define json_check_float(value, strKey) (json_check_is_float(value, strKey) ? ((value)[strKey]).GetFloat() : 0)
#define json_check_double(value, strKey) (json_check_is_double(value, strKey) ? ((value)[strKey]).GetDouble() : 0)

// 得到Value指针
#define json_check_value_ptr(value, strKey) (((value).HasMember(strKey)) ? &((value)[strKey]) : nullptr)

#define RapidJsonGetString(obj,out) \
    do { \
        rapidjson::StringBuffer buffer; \
        rapidjson::Writer<rapidjson::StringBuffer> writer(buffer); \
                            \
        obj.Accept(writer); \
        out = buffer.GetString();  \
    } while(0)

#define RapidJsonObjAddMember(d,obj,key,value) \
    do { \
        rapidjson::Document::AllocatorType& allocator = d.GetAllocator(); \
        obj.AddMember(key,value,allocator);  \
    } while(0)

#define RapidJsonObjAddStrMember(d,obj,key,value) \
    do { \
        rapidjson::Document::AllocatorType& allocator = d.GetAllocator(); \
        rapidjson::Value p1_val,v1_val; \
        p1_val.SetString(key,allocator);   \
        v1_val.SetString(value,allocator);   \
        obj.AddMember(p1_val,v1_val,allocator);  \
    } while(0)

#define RapidJsonObjAddStringMember(d,obj,key,value) \
    do { \
        rapidjson::Document::AllocatorType& allocator = d.GetAllocator(); \
        rapidjson::Value p1_val,v1_val; \
        p1_val.SetString(rapidjson::StringRef(key),allocator);   \
        v1_val.SetString(rapidjson::StringRef(value),allocator);   \
        obj.AddMember(p1_val,v1_val,allocator);  \
    } while(0)

#endif
