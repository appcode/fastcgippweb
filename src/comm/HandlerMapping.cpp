#include "HandlerMapping.h"

HandlerMapping::HandlerMapping(){

}

HandlerMapping* HandlerMapping::getInstance(){
	//c++11及其以后的编译器是线程安全的
	static HandlerMapping instance;

	return &instance;
}

/*
	@brief 添加url和HttpHandler映射关系
	@param url http请求的url
	@param httpHandler HttpHandler指针
 */
void HandlerMapping::addUrlMappingHandler(const std::string& url,FastCgiHandler* fastCgiHandler){
	if(fastCgiHandler != NULL){
		handlerMapping.insert(make_pair(url,fastCgiHandler));
	}
}

/*
	@brief 根据url查找相应的handler
	@param url请求的url
 */
FastCgiHandler* HandlerMapping::getHandler(const std::string& url){
	if(handlerMapping.count(url) > 0){
		return handlerMapping[url];
	}

	return NULL;
}