#include "ServerContext.h"
#include "IniFile.h"
#include "DBPool.h"
#include "RedisConnetionPool.h"

ServerContext::ServerContext(){

}

ServerContext::~ServerContext(){

}

Context* ServerContext::getInstance(){
	static ServerContext instance;

	return &instance;
}

void ServerContext::setContext(const char* key,void* value){
	if(strlen(key) > 0 && value != NULL){
		contextMap.insert(make_pair(std::string(key),value));
	}
}

void ServerContext::setContext(const std::string& key,void* value){
	if(key.length() > 0 && value != NULL){
		contextMap.insert(make_pair(key,value));
	}
}

void* ServerContext::getContext(const char* key){
	if(contextMap.count(key) > 0){
		return contextMap[key];
	}

	return NULL;
}

void* ServerContext::getContext(const std::string& key){
	if(contextMap.count(key) > 0){
		return contextMap[key];
	}

	return NULL;
}

/**
 * @brief 初始化ServerContext
 */
int ServerContext::initContext() {
    std::string conf_file = "../conf/app.ini";

    std::string db_ip,db_port,db_user,db_password,db_name,db_log_name,db_max_conn;
    std:;string redis_ip,redis_port,redis_auth;

    //////////////////////////////////////////////////////////////////////////////////////////
    // 数据库配置
    //////////////////////////////////////////////////////////////////////////////////////////

    db_ip = CIniFile::GetValue("ip","database_master",conf_file);
    db_port = CIniFile::GetValue("port","database_master",conf_file);
    db_user = CIniFile::GetValue("user","database_master",conf_file);
    db_password = CIniFile::GetValue("password","database_master",conf_file);
    db_name = CIniFile::GetValue("db","database_master",conf_file);
    db_log_name = CIniFile::GetValue("db_log","database_master",conf_file);
    db_max_conn = CIniFile::GetValue("max_conn","database_master",conf_file);

    CDBPool* dbpool_ad = new CDBPool("db_ad",db_ip.c_str(),std::atoi(db_port.c_str()),db_user.c_str()
                                  ,db_password.c_str(),db_name.c_str(),std::atoi(db_max_conn.c_str()));
    if(dbpool_ad->Init() != 0) {
        std::cout<<"init database fail!"<<std::endl;
        return -1;
    }

    CDBPool* dbpool_log_ad = new CDBPool("db_log_ad",db_ip.c_str(),std::atoi(db_port.c_str()),db_user.c_str()
                                  ,db_password.c_str(),db_log_name.c_str(),std::atoi(db_max_conn.c_str()));

    if(dbpool_log_ad->Init() != 0) {
        std::cout<<"init database fail!"<<std::endl;
        return -1;
    }

    this->setContext("dbpool_ad",static_cast<void*>(dbpool_ad));
    this->setContext("dbpool_log_ad",static_cast<void*>(dbpool_log_ad));



    db_ip = CIniFile::GetValue("ip","database_app",conf_file);
    db_port = CIniFile::GetValue("port","database_app",conf_file);
    db_user = CIniFile::GetValue("user","database_app",conf_file);
    db_password = CIniFile::GetValue("password","database_app",conf_file);
    db_name = CIniFile::GetValue("db","database_app",conf_file);
    db_log_name = CIniFile::GetValue("db_log","database_app",conf_file);
    db_max_conn = CIniFile::GetValue("max_conn","database_app",conf_file);

    CDBPool* dbpool_app = new CDBPool("db_app",db_ip.c_str(),std::atoi(db_port.c_str()),db_user.c_str()
                                  ,db_password.c_str(),db_name.c_str(),std::atoi(db_max_conn.c_str()));
    if(dbpool_app->Init() != 0) {
        std::cout<<"init database fail!"<<std::endl;
        return -1;
    }

    this->setContext("dbpool_app",static_cast<void*>(dbpool_app));


    //////////////////////////////////////////////////////////////////////////////////////////
    // 缓存配置
    //////////////////////////////////////////////////////////////////////////////////////////

    redis_ip = CIniFile::GetValue("ip","redis",conf_file);
    redis_port = CIniFile::GetValue("port","redis",conf_file);
    redis_auth = CIniFile::GetValue("auth","redis",conf_file);

    CacheConnectionPool* cache_pool =
            new RedisConnectionPool(redis_ip.c_str(),std::atoi(redis_port.c_str()),redis_auth.c_str());

    if(cache_pool->init() != 0) {
        std::cout<<"init redis fail!"<<std::endl;
        return -1;
    }

    this->setContext("cache_pool",cache_pool);

    return 0;
}
