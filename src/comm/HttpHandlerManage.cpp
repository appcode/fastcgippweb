#include "HttpHandlerManage.h"

HttpHandlerManage::HttpHandlerManage(){
	handlerMapping = HandlerMapping::getInstance();
}

HttpHandlerManage::~HttpHandlerManage(){
	handlerMapping = NULL;
}

void HttpHandlerManage::addHandlerMapping(const std::string& url,FastCgiHandler* fastCgiHandler){
	if(handlerMapping != NULL){
		handlerMapping->addUrlMappingHandler(url,fastCgiHandler);
	}
}

FastCgiHandler* HttpHandlerManage::getHandler(const std::string& url){
	if(handlerMapping != NULL){
		return handlerMapping->getHandler(url);
	}

	return NULL;
}