#include "controller/TestHandler.h"

#include "ServerContext.h"
#include "DbUtil.h"

TestHandler::TestHandler(const std::string& url):FastCgiHandler(url){

}

TestHandler::~TestHandler(){

}

void TestHandler::doGet(const Fastcgipp::Http::Environment<char>& env,std::basic_ostream<char>& resp){
    resp << "Content-Type: application/json; charset=utf-8\r\n\r\n";

    resp<<"Hello,TestHandler get!";

#if 0
	Context* ctx = ServerContext::getInstance();
	CConnectionPool* pool = static_cast<CConnectionPool*>(ctx->getContext("caipiao"));
	mysqlpp::ScopedConnection cp(*pool, true);

	mysqlpp::Query query = cp->query("select email from tb_user limit 1");

	mysqlpp::StoreQueryResult res = query.store();

	if(res){
		for (size_t i = 0; i < res.num_rows(); ++i){
			req.out << res[i][0];
		}
	}
#endif

#if 0
	Context* ctx = ServerContext::getInstance();
	CConnectionPool* pool = static_cast<CConnectionPool*>(ctx->getContext("caipiao"));

	std::string sql = "select appid from tb_user limit 1";

	mysqlpp::StoreQueryResult res = DbUtil::Select(pool,sql);
	if(res){
		for (size_t i = 0; i < res.num_rows(); ++i){
			req.out << res[i][0]<<std::endl;
		}
	}
#endif

}

void TestHandler::doPost(const Fastcgipp::Http::Environment<char>& env,std::basic_ostream<char>& resp){
    resp << "Content-Type: application/json; charset=utf-8\r\n\r\n";

    resp<<"Hello,TestHandler post!";
}
